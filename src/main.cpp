/*
frekvence, sf, bw, pream, crc on, coderate, mac pause

frekvence, sf, bw, watchdog, mac pause
*/

#include <Arduino.h>
#include "../lib/BUT_LoRa_lib.h"

#define Transmitter 1

#define RADIO_TX

#define BUFFER_SIZE 500

#define DEVADDR (char *) ""
#define DEVEUI  (char *) ""
#define NWKSKEY (char *) ""
#define APPSKEY (char *) ""

// Default parametrs
#define FRDEF 868100000
#define SFDEF 12
#define BWDEF 125
#define PLDEF 8
#define CRCDEF on
#define CRDEF 4/5
#define WindowSize 0


HardwareSerial &debugSerial = Serial;
HardwareSerial &loraSerial = Serial3;

char loraBuffer[BUFFER_SIZE] = {0};
uint8_t started = 0;
char *data;

but_lora lora(loraSerial, debugSerial, loraBuffer, BUFFER_SIZE);

void setup() {
  debugSerial.begin(9600);
  loraSerial.begin(57600);
  delay(3000);

}

void loop() {
  if(started == 0){
  #if Transmitter
    if(lora.radioGetFrequency() != FRDEF){  //set current frequency to 818.1MHz
      lora.radioSetFrequency(FRDEF);
    }
    if(lora.radioGetSpreadingFactor() != SFDEF){  //set SpreadingFactor to 12
      lora.radioSetSpreadingFactor(SF_12);
    }
    if(lora.radioGetBandWidth() != BWDEF){  //set BandWidth to 125
      lora.radioSetBandwidth(BW_125);
    }
    if(lora.radioGetPreambleLength() != PLDEF){  //set PreambleLength to 8
      lora.radioSetPreambleLength(PLDEF);
    }
    if(lora.radioGetcodingRate() != CR_4_5){  //set CodingRate to 4/5
      lora.radioSetCodingRate(CR_4_5);
    }

    uint32_t time; 
    lora.macPause(&time);
    data = new char[10];
    for(size_t i = 0; 0 < strlen(data); i++){
      data[i] = 'a';
    }
    lora.radioTx(data);
  #else
    if(lora.radioGetFrequency() != FRDEF){  //set current frequency to 818.1MHz
      lora.radioSetFrequency(FRDEF);
    }
    if(lora.radioGetSpreadingFactor() != SFDEF){  //set SpreadingFactor to 12
      lora.radioSetSpreadingFactor(SF_12);
    }
    if(lora.radioGetBandWidth() != BWDEF){  //set BandWidth to 125
      lora.radioSetBandwidth(BW_125);
    }
    if(lora.radioGetPreambleLength() != PLDEF){  //set PreambleLength to 8
      lora.radioSetPreambleLength(PLDEF);
    }
    if(lora.radioGetcodingRate() != CR_4_5){  //set CodingRate to 4/5
      lora.radioSetCodingRate(CR_4_5);
    }

    uint32_t time; 
    lora.macPause(&time);
    lora.radioRx(WindowSize, data);

  #endif

  delete[] data;
  started = 1;
  }
}